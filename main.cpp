#include "mainwindow.h"
#include <QApplication>
#include "log4qt/logger.h"
#include "log4qt/propertyconfigurator.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    Log4Qt::PropertyConfigurator::configure("./lus/log4qt.conf");
    MainWindow w;
    w.show();

    return a.exec();
}
