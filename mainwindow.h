#ifndef MAINWINDOW_H
#define MAINWINDOW_H



#include <QMainWindow>
#include <QTimer>
#include <qlist.h>

#include <QtXml>
#include <QtXml/QDomDocument>

#include "lus_db/lus_data_type.h"
#include "lus_db/lus_db.h"
#include "log4qt/logger.h"
#include "baidu_map.h"
#include "send_file.h"
#include "terminal_cmd.h"
#include "change_gid.h"
#include "dialog_comment.h"
#include "hpsocket_s.h"
#include "lus_st.h"

namespace Ui {
class MainWindow;
}

/*******配置宏********/

#define TXT_CACHE_SIZE  1000

/*******************/


class MainWindow : public QMainWindow
{
    Q_OBJECT
    LOG4QT_DECLARE_QCLASS_LOGGER
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void closeEvent(QCloseEvent *event);
    struct _comment{

        QString fix_id;
        QString comment;

    };
    QDateTime sys_start_date;
    QTime show_time;
    uint32_t show_day;
    /********************/

    bool debug_flag = true;
    hpsocket_s *server;
    lus_db *db;
	//aes_tcp_server *server = new aes_tcp_server(this);

    QTimer *timer_flush_ui;
    QTimer *timer_pubilc;

    QUdpSocket *udp_sender;

    lus_client_listp client_list;
    lus_client_listp offline_client_list;
    lus_client_listp down_list;

    send_file *send = new send_file;
    terminal_cmd *terminal = new terminal_cmd;
    change_gid * c_gid = new change_gid;
    Dialog_comment * commemt_win = new Dialog_comment;
    QByteArray local_pub_rsa;
    baidu_map * map = new baidu_map;

    void show_server_conf(void);

   QList<struct _comment *> comment_list;

    void licence_init(void);
    void log_printf(QString log_text);
    /*Yin版本发布系统*/
    /*file*/

private slots:

    void update_client_ip_slot();
    void show_client(struct m_client * p_clinet );
    void flush_bd_info_slot(CONNID fix_id);
    /*client 掉线清理*/
    void client_ui_clear(QString id);

    void timer_public_fun(void);
    void timer_flush_fun(void);

    void on_pushButton_new_version_clicked();

    void on_listView_clicked(const QModelIndex &index);

    void on_pushButton_send_file_clicked();

    void on_tableWidget_customContextMenuRequested(const QPoint &pos);

    void on_pushButton_send_cmd_clicked();

    void on_change_gid_click(bool);
    void on_offline_click(bool);
    void on_detail_click(bool);
    void on_comment_click(bool);

    void on_pushButton_top_info_clicked();

    void on_pushButton_flush_clicked();

    void on_pushButton_tree_clicked();

private:
    Ui::MainWindow *ui;

    //void read_conf(void);

   // void prase_group_conf(QDomElement child_element);
    void init_ui(void);

public slots:
    void send_terminal_cmd(QString str_cmd);

};

#endif // MAINWINDOW_H
