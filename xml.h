#ifndef XML_H
#define XML_H

#include <qlist.h>
#include <QtXml>
#include <QtXml/QDomDocument>


class xml
{


private:

    struct _version_inf{

        QString group_id;
        QString file_path;
        QString version;

    };


public:




    struct _server_conf{

        QList<struct _version_inf *> version_inf_list;
        QString max_down;

    };

    struct _server_conf *server_conf;

    struct _version_inf *v_inf = new struct _version_inf;


   void  read_conf(QString path);
   void  add_new_version(void);


private:

    void prase_group_conf(QDomElement child_element);
    void prase_down_conf(QDomElement child_element);

    QString xml_path;

};

#endif // XML_H
