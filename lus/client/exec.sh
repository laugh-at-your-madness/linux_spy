#!/bin/sh


app=mirror
w_dir=/work/

if test -e $app
then
	echo "pwd: "`pwd`
	chmod +x $app
	mv "${w_dir}${app}" "${w_dir}${app}.bak"
	mv $app $w_dir
	sync
	reboot
else
		echo "not found file" > ${w_dir}ex.log
fi
