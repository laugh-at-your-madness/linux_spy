
# linux SPY

## 简介

使用Qt开发的linux嵌入式设备监控、管理框架
[客户端仓库](https://gitee.com/zhaojun_chao/aes_tcp_lib)
## 核心功能
* 监测嵌入式设备运行状态
* 转发客户端消息, 为客户端提供消息转发服务
* 对设备执行远程shell
* 对设备进行远程升级，管理设备固件版本
* 与客户端进行文件传输

![主界面图](./lus/linux_spy_main.png)
![设备上线](./lus/lus.png)
![设备树形](./lus/lus-2.png)

# 第三方依赖

- https://gitee.com/ldcsaa/HP-Socket
- https://github.com/MEONMedical/Log4Qt