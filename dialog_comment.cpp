#include "dialog_comment.h"
#include "ui_dialog_comment.h"
#include <QMessageBox>

Dialog_comment::Dialog_comment(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog_comment)
{
    ui->setupUi(this);
}

Dialog_comment::~Dialog_comment()
{
    delete ui;
}

void Dialog_comment::on_pushButton_ok_clicked()
{
    QString text = ui->lineEdit->text();

    if(text.isEmpty()){


       close();
    }

    comment = text;

    ui->lineEdit->setText("");
    close();

}
