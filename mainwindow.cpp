#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>
#include <QListView>
#include <QStandardItem>
#include <QStandardItemModel>
#include <QModelIndex>
#include <QtGlobal>
#include <QtGlobal>

#include <QFile>
#include <QDirIterator>
#include <QFileDialog>
#include <QMenu>

#include <QMessageBox>


/*
 *
 * */

/*界面tableWidget id*/
enum TABLE_POINT{
    CHECK_POINT = 0,
    GROUPID_POINT,
    ID_POINT,
    IP_POINT,
    TEMP_POINT,
    CPU_POINT,
    TIME_POINT,
    PLACE_POINT,
    VER_POINT,
    COMMENT_POINT,

};


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    logger()->info("start system");
	db = new lus_db;
	db->lus_database_init();
	
    server = new hpsocket_s;
    server->lus_init_conf("./lus/lus_server.conf");
    client_list = server->lus_get_client_list();
    down_list = server->lus_get_down_list();
    offline_client_list = server->lus_get_offline_client_list();

    /*用于根据ip，查询物理地址*/
    map->baidu_map_init();

    /*post 返回信息*/
    connect(map, SIGNAL(ip_to_addr_sig()),this,SLOT(update_client_ip_slot()));

    /*terminal 信号槽*/
    connect(server, SIGNAL(terminal_bak(QString)),terminal,SLOT(get_terminal_bak(QString)));
    connect(terminal, SIGNAL(terminal_cmd_ready(QString)),this,SLOT(send_terminal_cmd(QString)));

    init_ui();

    /*test*/

    /*end test*/

    show_server_conf();

    /*公用定时器初始化*/
    timer_pubilc = new QTimer(this);;
    connect( timer_pubilc, SIGNAL( timeout() ), this, SLOT( timer_public_fun() ) );
    timer_pubilc->start(1000);   //1s
    logger()->info("start pubilc timer");

    timer_flush_ui = new QTimer(this);
    connect(timer_flush_ui, SIGNAL(timeout()), this, SLOT(timer_flush_fun()));
    timer_flush_ui->start(5000);
    logger()->info("start flush ui timer");

    udp_sender = new QUdpSocket(this);
    sys_start_date = QDateTime::currentDateTime();

    show_day = 0;
}

MainWindow::~MainWindow()
{

    delete ui;
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    int button;
    button = QMessageBox::question(this, tr("check"),
                                   QString(tr("exit?")),
                                   QMessageBox::Yes | QMessageBox::No);
    if (button == QMessageBox::No) {
          event->ignore();
    }
    else if (button == QMessageBox::Yes) {
          event->accept();
    }
}

void MainWindow::timer_flush_fun(void)
{
    /*add offline client log*/
    for(int i=0; i<offline_client_list->size(); i++){
        _st_date date;
        m_client * client = offline_client_list->at(i);

        date.fix_id = client->fix_id;
        date.up_date = client->up_time.toString("M-d hh:m:s");
        date.down_date = client->down_time.toString("M-d hh:m:s ");

        db->lus_store_requst((void *)&date, LUS_TY_UPTIME);
        offline_client_list->removeOne(client);
    }

    /*add comment*/
    for(int i=0; i<client_list->size(); i++){

	
	}
}
void MainWindow::timer_public_fun(void)
{
    QDateTime now_date = QDateTime::currentDateTime();
    show_time.setHMS(0,0,0,0);
    show_time = show_time.addSecs((int)sys_start_date.secsTo(now_date));
    if(!show_time.isNull()){
        if((show_time.hour() ==24) && (show_time.minute()==59)){
           sys_start_date = QDateTime::currentDateTime();
           show_day++;
        }
        ui->lcd_clock->display(QString::number(show_day) +"-"+show_time.toString("hh:mm:ss"));
    }

    /*清理ui缓冲*/
    if(ui->textBrowser->toPlainText().size() > TXT_CACHE_SIZE) ui->textBrowser->clear();
    ui->label_user_num->setText(QString::number(server->lus_client_msg_count()));
    ui->label_down_num->setText(QString::number(server->lus_client_down_count()));
}

void MainWindow::update_client_ip_slot()
{
    for(int i=0; i<ui->tableWidget->rowCount(); i++){

        m_client * client = server->id_2_client(ui->tableWidget->item(i, ID_POINT)->text().toInt());
        CHECK_PTR_VOID(client);
        if(client->fix_id == map->post_ack.fix_id){
            ui->tableWidget->item(i, PLACE_POINT)->setText(map->post_ack.addr);
            client->ip2place = map->post_ack.addr;
            logger()->info("update client "+client->fix_id+" addr info: "+map->post_ack.addr);
        }
    }
}

/*发送shell命令*/
//TODO
void MainWindow::send_terminal_cmd(QString str_cmd)
{
    for(int i=0; i<ui->tableWidget->rowCount(); i++){

           if(ui->tableWidget->item(i, CHECK_POINT)->checkState() == Qt::Checked){

                    QString str_id =    ui->tableWidget->item(i, ID_POINT)->text();
                    m_client *client = server->id_2_client(str_id.toInt());
                    CHECK_PTR_VOID(client);
                    QString log = "send termin cmd:" + str_cmd +" to " +client->fix_id;

                    logger()->info(log);
                    ui->textBrowser->append(log);
                    server->sendMessage(str_id.toInt(), str_cmd.toLatin1(), ENCRYPT_YES);
           }

       }
}

void MainWindow::init_ui(void)
{

    logger()->info("init ui");
    ui->tableWidget->setColumnWidth(CHECK_POINT, 40);
    ui->tableWidget->setColumnWidth(GROUPID_POINT, 40);
    ui->tableWidget->setColumnWidth(ID_POINT, 65);
    ui->tableWidget->setColumnWidth(IP_POINT, 100);
    ui->tableWidget->setColumnWidth(TEMP_POINT, 200);
    ui->tableWidget->setColumnWidth(COMMENT_POINT, 65);

    QIcon icon;
    icon.addFile(tr(":/res/app_send.ico"));
    ui->pushButton_send_file->setIcon(icon);
    ui->pushButton_send_file->setText("");
    ui->pushButton_send_file->setFlat(true);
    ui->pushButton_send_file->setIconSize(QSize(75, 75));
    ui->pushButton_send_file->setToolTip("发送文件");

    icon.addFile(tr(":/res/app_edit.ico"));
    ui->pushButton_send_cmd->setIcon(icon);
    ui->pushButton_send_cmd->setText("");
    ui->pushButton_send_cmd->setFlat(true);
    ui->pushButton_send_cmd->setIconSize(QSize(75, 75));
    ui->pushButton_send_cmd->setToolTip("命令终端");

    icon.addFile(tr(":/res/app_search.ico"));
    ui->pushButton_serch->setIcon(icon);
    ui->pushButton_serch->setText("");
    ui->pushButton_serch->setFlat(true);
    ui->pushButton_serch->setIconSize(QSize(75, 75));
    ui->pushButton_serch->setToolTip("nop");

    icon.addFile(tr(":/res/app_info.ico"));
    ui->pushButton_top_info->setIcon(icon);
    ui->pushButton_top_info->setText("");
    ui->pushButton_top_info->setFlat(true);
    ui->pushButton_top_info->setIconSize(QSize(75, 75));
     ui->pushButton_top_info->setToolTip("查看设备信息");

     QPixmap image_init;
     image_init.load(":/res/about.png");
     ui->label_about->setPixmap(image_init);

     ui->tableWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);
     ui->tableWidget->setFrameShape(QFrame::NoFrame);
     ui->tableWidget-> setSelectionBehavior(QAbstractItemView::SelectRows);

    ui->treeWidget->setHeaderHidden(true);
}

void MainWindow:: show_server_conf(void)
{

    logger()->info("show server configure");
    ui->lineEdit_max_down->setText(server->xml_conf->server_conf->max_down);

   QStandardItemModel * standardItemModel = new QStandardItemModel(this);

        QStringList strList;

        for(int i=0; i < server->xml_conf->server_conf->version_inf_list.count(); i++){

            strList.append("组ID:"+ server->xml_conf->server_conf->version_inf_list.at(i)->group_id + "  版本号:" \
                           + server->xml_conf->server_conf->version_inf_list.at(i)->version);

            ui->comboBox_group->addItem("组ID:"+ server->xml_conf->server_conf->version_inf_list.at(i)->group_id);
        }

        //版本信息list显示
        int nCount = strList.size();
        for(int i = 0; i < nCount; i++)
        {
            QString string = static_cast<QString>(strList.at(i));
            QStandardItem *item = new QStandardItem(string);
            if(i % 2 == 1)
            {
                QLinearGradient linearGrad(QPointF(0, 0), QPointF(200, 200));
                linearGrad.setColorAt(0, Qt::darkGreen);
                linearGrad.setColorAt(1, Qt::yellow);
                QBrush brush(linearGrad);
                item->setBackground(brush);
            }
            standardItemModel->appendRow(item);
        }
        ui->listView->setModel(standardItemModel);
        ui->listView->setFixedSize(200,300);
}

/*tablewidget 创建client 并显示IP，物理地址*/
void MainWindow::show_client(struct m_client * p_clinet )
{

    QString ip_str =p_clinet->clinet_ip;
    post_ip_info ip_info;
    logger()->debug("show client : "+p_clinet->fix_id);

    ui->tableWidget->insertRow(ui->tableWidget->rowCount());
    ui->tableWidget->setItem(ui->tableWidget->rowCount() - 1, ID_POINT, new QTableWidgetItem(QString::number(p_clinet->var_id)));

    QTableWidgetItem *check_box = new QTableWidgetItem();
    check_box->setCheckState(Qt::Unchecked);
    ui->tableWidget->setItem(ui->tableWidget->rowCount() - 1, CHECK_POINT, check_box);

    QString time_str = p_clinet->up_time.toString("M-d hh:m:s");
    ui->tableWidget->setItem(ui->tableWidget->rowCount() - 1, TIME_POINT, new QTableWidgetItem(time_str));

    ui->tableWidget->setItem(ui->tableWidget->rowCount() - 1, IP_POINT, new QTableWidgetItem(ip_str));

    ui->tableWidget->setItem(ui->tableWidget->rowCount() - 1, COMMENT_POINT, new QTableWidgetItem("NULL"));

    logger()->info("start ip to addr "+p_clinet->fix_id +"<->"+p_clinet->clinet_ip);
    if(p_clinet->ip2place.isEmpty()){
        ip_info.fix_id=p_clinet->fix_id;
        ip_info.ip = p_clinet->clinet_ip;
        qDebug()<<ip_info.fix_id <<ip_info.ip;
        map->ip_to_addr(ip_info);
        ui->tableWidget->setItem(ui->tableWidget->rowCount() - 1, PLACE_POINT, new QTableWidgetItem("waiting"));
    }else{
        ui->tableWidget->setItem(ui->tableWidget->rowCount() - 1, PLACE_POINT, new QTableWidgetItem(p_clinet->ip2place));
    }
}

/*从table删除disconnected 的 client*/
void MainWindow::client_ui_clear(QString id)
{
    for(int i=0; i<ui->tableWidget->rowCount(); i++){
        logger()->debug("clear client id:"+id);
            if(ui->tableWidget->item(i, ID_POINT)->text() == id){
                ui->tableWidget->removeRow(i);
                logger()->debug("found and clear ui,id: "+id);
            }
    }

}

/*发布新的版本信息*/
void MainWindow::on_pushButton_new_version_clicked()
{

    if(ui->lineEdit_version_num->text().isEmpty()){

            QMessageBox::information(this,"错误","请输入版本号");
            return ;
    }

    if(ui->lineEdit_group_id->text().isEmpty()){

            QMessageBox::information(this,"错误","请输入组ID");
            return ;
    }


   QString path=QFileDialog::getOpenFileName(this,"选择文件","./Phone","update pack(*.zip);;tel(*.sh)");

   if(path.isEmpty())
        return;
    logger()->info("add new version path:" + path);

   server->xml_conf->v_inf->file_path = path;
   server->xml_conf->v_inf->group_id = ui->lineEdit_group_id->text();
   server->xml_conf->v_inf->version = ui->lineEdit_version_num->text();

   server->xml_conf->add_new_version();

   //update show
#ifdef Q_OS_WIN32
       server->xml_conf->read_conf("./lus_server_conf.xml");
       server->log_printf("windows system");
#endif

    //TODO: fix it
#ifdef Q_OS_LINUX
       server->xml_conf->read_conf("/etc/lus/lus_server_conf.xml");
       server->log_printf("linux system");
#endif

}

void MainWindow::on_listView_clicked(const QModelIndex &index)
{

    QString str =  ui->listView->model()->index(index.row(), 0).data().toString();

    str = str.left(str.indexOf(" "));

    str = str.right(str.size() - str.indexOf(":") - 1);

    ui->label_version_path->setText(server->xml_conf->server_conf->version_inf_list.at(index.row())->file_path);

    ui->lineEdit_group_id->setText(str);
}

void MainWindow::on_pushButton_send_file_clicked()
{

    QString path=QFileDialog::getOpenFileName(this,"选择文件","./Phone","update(*.zip);;all(*)");

    if(path.isEmpty()){

        ui->textBrowser->append("file path error!");
        return;
    }

    send->exec();

    QString cmd;

    cmd =  "<lus_cmd body=\"send_file\" ";
    cmd += " type=\"" + send->file_mark.str_file_type + "\"";

    logger()->info("send file cmd: "+cmd);

    if(send->file_mark.enabled_md5){
        QString md5 = server->get_file_md5(path);
         cmd += " md5=\""+ md5 + "\">";
         logger()->info("send file md5 check :"+md5);
    }else{
         cmd += " md5=\"\">";
    }


    for(int i=0; i<ui->tableWidget->rowCount(); i++){

        if(ui->tableWidget->item(i, CHECK_POINT)->checkState() == Qt::Checked){

            QString str_id = ui->tableWidget->item(i, ID_POINT)->text();
            lus_client_st *client = server->id_2_client(str_id.toInt());
            CHECK_PTR_VOID(client);
            logger()->debug("send file to "+client->fix_id+ " "+path);
            client->down_info.file_path = path;
            client->down_info.type = send->file_mark.file_type;
            client->down_info.status = DOWN_WAITE;

            ui->textBrowser->append(cmd);

            server->sendMessage(str_id.toInt(), cmd.toLatin1(), ENCRYPT_YES);

        }
    }

}

void MainWindow::flush_bd_info_slot(CONNID var_id)
{
    struct m_client * client;

    qDebug()<<"flush client list";
    client = server->id_2_client(var_id);
    CHECK_PTR_VOID(client);
    for(int i=0; i<ui->tableWidget->rowCount(); i++){
        // add comment
        if(ui->tableWidget->item(i, COMMENT_POINT)->text() == "NULL"){

            for(int i=0; i<comment_list.size(); i++){

              //  if(comment_list.at(i)->fix_id ==  client->fix_id){

               //     ui->tableWidget->setItem(ui->tableWidget->rowCount() - 1, COMMENT_POINT, new QTableWidgetItem(comment_list.at(i)->comment));

              //  }

            }
        }

        if(ui->tableWidget->item(i, ID_POINT)->text().toInt() == client->var_id){
                ui->tableWidget->setItem(i, TEMP_POINT, new QTableWidgetItem(QString::number(client->bd_info.cpu_temp.toFloat()/1000)));
                ui->tableWidget->setItem(i, CPU_POINT, new QTableWidgetItem(client->bd_info.cpu_info));
                ui->tableWidget->setItem(i, GROUPID_POINT, new QTableWidgetItem(client->group_id));
                ui->tableWidget->setItem(i, VER_POINT, new QTableWidgetItem(client->firmware_ver));

            break;
        }
    }
}

//todo
void MainWindow::on_change_gid_click(bool)
{
    QString cmd = "<lus_cmd body=\"change_gid\" para1=\"xxxx\" />";
    QString id;

    qDebug() << "on_change_gid_click";
    c_gid->exec();

    if(c_gid->new_gid.isEmpty())return;

    for(int i=0; i<ui->tableWidget->rowCount(); i++){

        if(ui->tableWidget->item(i,ID_POINT)->isSelected()){

              id = ui->tableWidget->item(i, ID_POINT)->text();

                cmd.replace(cmd.indexOf("xxxx"), 4, c_gid->new_gid);
                server->sendMessage(id.toInt(), cmd.toLatin1(), ENCRYPT_YES);
                qDebug() << cmd;
        }

    }
    c_gid->new_gid.clear();
}

void MainWindow::on_offline_click(bool)
{

    QString id = 0;
    QByteArray cmd ="<lus_cmd body=\"ruin\" />";
    for(int i=0; i<ui->tableWidget->rowCount(); i++){

        if(ui->tableWidget->item(i,ID_POINT)->isSelected()){

            id = ui->tableWidget->item(i, ID_POINT)->text();
            m_client *client = server->id_2_client(id.toInt());
            CHECK_PTR_VOID(client);
            logger()->info("offline: "+id+" "+client->fix_id);
            //TODO get all run time
            server->sendMessage(id.toInt(), cmd, ENCRYPT_YES);
            client_ui_clear(id);
        }

    }


}

void MainWindow::on_detail_click(bool)
{
    QString id = 0;

    for(int i=0; i<ui->tableWidget->rowCount(); i++){

        if(ui->tableWidget->item(i,ID_POINT)->isSelected()){

            QString info_str;
            info_str.clear();

              id = ui->tableWidget->item(i, ID_POINT)->text();
              lus_client_st * client =  server->id_2_client(id.toInt());
              CHECK_PTR_VOID(client);
              info_str.append("<<<<<<<<<<<<<<<<<<<<<<<<");
              info_str.append("\n");
              info_str.append(client->fix_id);
              info_str.append("\n");
              info_str.append("var id: " +QString::number(client->var_id));
              info_str.append("\n");
              info_str.append("ip: " + client->clinet_ip);
              info_str.append("\n");
              info_str.append("cpu temp: " + client->bd_info.cpu_temp);
              info_str.append("\n");
              info_str.append("msg count: " + QString::number(client->msg_count));
              info_str.append("\n");
              info_str.append("msg list size: " + QString::number(server->lus_client_msg_count()));
              info_str.append("\n");
              info_str.append("down list size: " + QString::number(server->lus_client_down_count()));
              info_str.append("\n<<<<<<<<<<<<<<<<<<<<<<<<");
              ui->textBrowser->append(info_str);

        }

    }

}

//TODO
void MainWindow::on_comment_click(bool)
{

    QString id = "";

    commemt_win->exec();

    if(commemt_win->comment.isEmpty()){
        return;
    }

    for(int i=0; i<ui->tableWidget->rowCount(); i++){

        if(ui->tableWidget->item(i,ID_POINT)->isSelected()){

            int count=0;
            struct _comment *cm = new _comment;

            qDebug()<<"comment " <<i;
            id = ui->tableWidget->item(i, ID_POINT)->text();
            lus_client_st * client = server->id_2_client(id.toInt());
            CHECK_PTR_VOID(client);
            cm->fix_id = client->fix_id;
            cm->comment = commemt_win->comment;
            commemt_win->comment.clear();

            ui->tableWidget->setItem(i, COMMENT_POINT, new QTableWidgetItem(cm->comment));

            for(count=0; count<comment_list.size(); count++){

                if(comment_list.at(count)->fix_id == cm->fix_id){

                    comment_list.at(count)->comment = cm->comment;

                    break;
                }
            }

            //not found in comment list
            if(count == comment_list.size())
                    comment_list.append(cm);

        }

    }
}

/*右键菜单*/
void MainWindow::on_tableWidget_customContextMenuRequested(const QPoint &pos)
{

    QMenu *cmenu = new QMenu(ui->tableWidget);
    QAction *action1_change_gid = cmenu->addAction("修改组ID");
    QAction *action2_offline = cmenu->addAction("offline");
    QAction *action3 = cmenu->addAction("detail");
    QAction *action4 = cmenu->addAction("comment");

    connect(action1_change_gid, SIGNAL(triggered(bool)), this, SLOT(on_change_gid_click(bool)));
    connect(action2_offline, SIGNAL(triggered(bool)), this, SLOT(on_offline_click(bool)));
    connect(action3, SIGNAL(triggered(bool)), this, SLOT(on_detail_click(bool)));
    connect(action4, SIGNAL(triggered(bool)), this, SLOT(on_comment_click(bool)));

    cmenu->exec(QCursor::pos());
}

void MainWindow::on_pushButton_send_cmd_clicked()
{
    terminal->show();
}

void MainWindow::on_pushButton_top_info_clicked()
{

    /*
    qDebug()<<"licence list size: "<<server->licence_list.size();

    for(int i=0; i<server->licence_list.size(); i++)
        qDebug()<<"		"<<server->licence_list.at(i)->licence_str << " : " << server->licence_list.at(i)->key;


    qDebug()<<"clience list size: "<<server->msg_clinet_list.size();

    for(int i=0; i<server->msg_clinet_list.size(); i++){
        qDebug()<<"		var id:"<<server->msg_clinet_list.at(i)->var_id;
        qDebug()<<"		fix id:"<<server->msg_clinet_list.at(i)->fix_id;
        qDebug()<<"		msg count:"<<server->msg_clinet_list.at(i)->msg_count;
    }
    */
}

void MainWindow::on_pushButton_flush_clicked()
{

    int count = ui->tableWidget->rowCount();
     for(int i=0; i<count+1; i++){
        ui->tableWidget->removeRow(0);
    }

     for(int i=0; i<client_list->size(); i++){
        show_client(client_list->at(i));
        flush_bd_info_slot(client_list->at(i)->var_id);
    }
}

void MainWindow::on_pushButton_tree_clicked()
{
    QList<QTreeWidgetItem *> rootList;

    ui->treeWidget->clear();

    if(ui->comboBox_select_tree->currentText() == "msg client"){
        for(int i=0; i<client_list->size(); i++){
            QTreeWidgetItem *imageItem1 = new QTreeWidgetItem;
            m_client * client =client_list->at(i);

            imageItem1->setText(0,QString(QString("CNT ")+QString::number(client->var_id)));

            rootList.append(imageItem1);
            QString fix_id ="fix_id: ";
            fix_id += client->fix_id;
            QTreeWidgetItem *imageItem1_1 = new QTreeWidgetItem(imageItem1,QStringList(fix_id));
            imageItem1->addChild(imageItem1_1);

            QString up_time = "up time: ";
            up_time += client->up_time.toString("M-d hh:m:s");;
            QTreeWidgetItem *imageItem1_3 = new QTreeWidgetItem(imageItem1,QStringList(up_time));
            imageItem1->addChild(imageItem1_3);

            QString heart = "heart: ";
            heart += QString::number(client->heart);
            QTreeWidgetItem *imageItem1_4 = new QTreeWidgetItem(imageItem1,QStringList(heart));
            imageItem1->addChild(imageItem1_4);

            QString msg_count = "msg_count : ";
            msg_count += QString::number(client->msg_count);
            QTreeWidgetItem *imageItem1_5 = new QTreeWidgetItem(imageItem1,QStringList(msg_count));
            imageItem1->addChild(imageItem1_5);

            QString borad = "borad info";
            QTreeWidgetItem *borad_info = new QTreeWidgetItem(imageItem1,QStringList(borad));
            imageItem1->addChild(borad_info);

            QString cpu_tmp ="cpu temp: ";
            cpu_tmp += client->bd_info.cpu_temp;
            QTreeWidgetItem *imageItem2_1 = new QTreeWidgetItem(borad_info,QStringList(cpu_tmp));
            imageItem1->addChild(imageItem2_1);

            QString hd ="hard disk info: ";
            hd += client->bd_info.hd_info;
            QTreeWidgetItem *imageItem2_2 = new QTreeWidgetItem(borad_info,QStringList(hd));
            imageItem1->addChild(imageItem2_2);

            QString cpu ="cpu info: ";
            cpu += client->bd_info.cpu_info;
            QTreeWidgetItem *imageItem2_3 = new QTreeWidgetItem(borad_info,QStringList(cpu));
            imageItem1->addChild(imageItem2_3);

        }
    }else{

        QString down_status_str[5] = {"downing", "download ok", "download wating", "download error", "download disbale"};
        QString down_type_str[3] = {"pack", "file"};

        for(int i=0; i<down_list->size(); i++){

            QTreeWidgetItem *imageItem1 = new QTreeWidgetItem;
            m_client * client =down_list->at(i);
            imageItem1->setText(0,QString(QString("DOWN ")+QString::number(client->var_id)));
            rootList.append(imageItem1);

            if(client->fix_id.isEmpty()){

                QTreeWidgetItem *imageItem3_1 = new QTreeWidgetItem(imageItem1,QStringList("no down info"));
                imageItem1->addChild(imageItem3_1);

                QString heart ="file cnt heart:";
                heart += QString::number(client->heart);
                QTreeWidgetItem *imageItem3_2 = new QTreeWidgetItem(imageItem1,QStringList(heart));
                imageItem1->addChild(imageItem3_2);

            }else{

                QString path ="file path:";
                path += client->down_info.file_path;
                QTreeWidgetItem *imageItem3_1 = new QTreeWidgetItem(imageItem1,QStringList(path));
                imageItem1->addChild(imageItem3_1);

                QString status ="status:";
                status +=down_status_str[client->down_info.status];
                QTreeWidgetItem *imageItem3_2 = new QTreeWidgetItem(imageItem1,QStringList(status));
                imageItem1->addChild(imageItem3_2);

                QString type ="type:";
                type +=down_type_str[client->down_info.type];
                QTreeWidgetItem *imageItem3_3 = new QTreeWidgetItem(imageItem1,QStringList(type));
                imageItem1->addChild(imageItem3_3);
            }
        }

    }
    // for(int i=0; i<down_list->size(); i++){

    //}
    ui->treeWidget->insertTopLevelItems(0,rootList);
}
