#ifndef HPSOCKET_S_H
#define HPSOCKET_S_H

#include <hpsocket/HPSocket.h>
#include <QObject>
#include <QFile>
#include <QDomDocument>
#include <QMap>
#include <QTcpSocket>
#include <QTcpServer>
#include <QFileInfo>
#include <openssl/rsa.h>
#include <openssl/pem.h>

#include "qt_aes.h"
#include "xml.h"
#include "lus_st.h"
#include "log4qt/logger.h"
#define likely(x)    __builtin_expect(!!(x), 1)
#define unlikely(x)    __builtin_expect(!!(x), 0)
#define FILE_MSG_CACHE 1024*10
#define CRYPT_CACHE_SIZE 1024*10
#define CHECK_PTR_VOID(x) do{if((x)==NULL){logger()->error()<<__FUNCTION__<<" PTR NULL!";return;}}while(0)
#define CHECK_PTR(x) do{if((x)==NULL){logger()->error()<<__FUNCTION__<<" PTR NULL!";return -1;}}while(0)

#define DEBUG_SOCKET 1

#define LUS_PACK_HEAH_SZ ((int)(sizeof(quint32)*2 +sizeof(quint8)))

class CListenerImpl : public QObject, public CTcpPullServerListener, public lus_log
{
    Q_OBJECT
    LOG4QT_DECLARE_QCLASS_LOGGER
public:
//    virtual EnHandleResult OnPrepareListen(ITcpServer* pSender, SOCKET soListen) override;
    virtual EnHandleResult OnAccept(ITcpServer* pSender, CONNID dwConnID, UINT_PTR soClient) override;
  //virtual EnHandleResult OnHandShake(ITcpServer* pSender, CONNID dwConnID) override;
    virtual EnHandleResult OnClose(ITcpServer* pSender, CONNID dwConnID, EnSocketOperation enOperation, int iErrorCode) override;
    virtual EnHandleResult OnReceive(ITcpServer* pSender, CONNID dwConnID, int iLength) override;
    virtual EnHandleResult OnSend(ITcpServer* pSender, CONNID dwConnID, const BYTE* pData, int iLength) override;

    void init_listener(void);
    void lus_send(ITcpServer* pSender, CONNID dwConnID, QByteArray b_data, quint8 encrypt_c=ENCRYPT_YES);

    const ITcpPullServer * hp_msg_server;
    const ITcpPullServer * hp_file_server;
    QList<struct m_client *> msg_clinet_list;
    QMap<CONNID, struct m_client *>client_map;
    QList<struct m_client *> offline_list;
    TAesClass *aes = new TAesClass;

    //RSA *pub_rsa;
    QDomDocument server_xml;
    QByteArray local_pub_rsa;
    RSA *lus_rsa = nullptr;
    BIGNUM *bne = nullptr;
    char *rsa_cache = nullptr;
private:
    UCHAR aes_key[16] = {0x22, 0xa2, 0x13, 0xe2, 0x22, 0xa2, 0x12, 0x20, 0x22, 0x15, 0x22, 0xda, 0x67, 0x22, 0x32, 0x22 };
    QTimer *pub_timer;
    void make_client_pub(struct m_client*client, QString pub_str);
    QByteArray outBlock;    //数据缓冲区，即存放每次要发送的数据
    void sendFileBody(qint64 numBytes, CONNID dwConnID);
    void do_cmd(QByteArray cmd, CONNID dwConnID, uint8_t encrypt_c);
    void email_data(QByteArray data, QString addr, uint8_t encrypt_c, int port);
signals:
    void lus_msg(QByteArray cmd, CONNID dwConnID, uint8_t encrypt_c);
private slots:
    void timer_public_fun(void);

};

class hpsocket_s : public QObject, public lus_log
{
    Q_OBJECT
    LOG4QT_DECLARE_QCLASS_LOGGER
public:
    hpsocket_s();

    void lus_init_conf(QString);
    lus_client_listp lus_get_client_list(void);
    lus_client_listp lus_get_offline_client_list(void);
    lus_client_listp lus_get_down_list(void);
    void sendMessage(CONNID dwConnID,  QByteArray b_data ,quint8 encrypt_c);
    lus_client_st* id_2_client(CONNID id);
    quint64 lus_client_msg_count(void);
    quint64 lus_client_down_count(void);
    quint16 downing_count;
    //void sendFile(struct m_client * p_clinet, QString fileName);
    xml * xml_conf = new xml;
    QString file_pool = "/etc/lus/file_pool/";
private:
    void printf_log(QString);
    QList<struct m_licence *> licence_list;
    QList<struct m_client *> file_clinet_list;
    ITcpPullServer * hp_msg_server;
    ITcpPullServer * hp_file_server;
    CListenerImpl *hp_listener;
    QTcpServer  *tcp_file_server;
    QTimer *timer_down;

    void lus_send_file(CONNID id, QString fileName);
    void sendfile_launch(struct m_client * p_clinet);
    /*文件收发相关*/
    QByteArray outBlock;    //数据缓冲区，即存放每次要发送的数据
    qint64 loadSize;   //每次发送数据的大小
    void prase_bd_info( QDomElement rootnode, CONNID dwConnID);
    /*end*/

signals:
    void terminal_bak(QString str);
    void new_client(struct m_client * client);
    void bury_client_signal(QString id);
    void flush_bd_info_signal(CONNID var_id);
private slots:
    void lus_do_cmd(QByteArray cmd, CONNID dwConnID, uint8_t encrypt_c);
    void sendFileBody(qint64 numBytes);
    void auto_scanf_down(void);
    void readsocket_data();
    void send_init_file();
    void clr_file_client();
};

#endif // HPSOCKET_S_H
