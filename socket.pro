#-------------------------------------------------
#
# Project created by QtCreator 2018-04-13T16:13:45
#
#-------------------------------------------------

QT       += core gui
QT       += network
QT       += xml
QT 	 += sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = socket
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += main.cpp\
    dialog_comment.cpp \
    hpsocket_s.cpp \
    lus_db/lus_db.cpp \
        mainwindow.cpp \
    qt_aes.cpp \
    xml.cpp \
    baidu_map.cpp \
    send_file.cpp \
    terminal_cmd.cpp \
    change_gid.cpp

HEADERS  += mainwindow.h \
    dialog_comment.h \
    hpsocket_s.h \
    lus_db/lus_data_type.h \
    lus_db/lus_db.h \
    lus_st.h \
    qt_aes.h \
    xml.h \
    baidu_map.h \
    send_file.h \
    terminal_cmd.h \
    change_gid.h

FORMS    += mainwindow.ui \
    dialog_comment.ui \
    send_file.ui \
    terminal_cmd.ui \
    change_gid.ui

RESOURCES += \
    res.qrc

DISTFILES +=

unix:!macx: LIBS += -L$$PWD/lib/ -lhpsocket

INCLUDEPATH += $$PWD/.
DEPENDPATH += $$PWD/.

unix:!macx: PRE_TARGETDEPS += $$PWD/lib/libhpsocket.a

unix:!macx: LIBS += -L$$PWD/lib/ -lhpsocket_d

INCLUDEPATH += $$PWD/.
INCLUDEPATH += $$PWD/include
DEPENDPATH += $$PWD/.

unix:!macx: PRE_TARGETDEPS += $$PWD/lib/libhpsocket_d.a

unix:!macx: LIBS += -L$$PWD/lib/ -llog4qt

INCLUDEPATH += $$PWD/include
DEPENDPATH += $$PWD/include




#unix:!macx: LIBS += -L/usr/lib64/ -lssl -lcrypto
unix:!macx: LIBS +=  -lssl -lcrypto


#unix:!macx: LIBS += -L$$PWD/../../../../../usr/local/lib64/ -lssl -lcrypto

#INCLUDEPATH += $$PWD/../../../../../usr/local/lib64
#DEPENDPATH += $$PWD/../../../../../usr/local/lib64
